(in-package :cl-user)
(defpackage chatai-server-test-asd
  (:use :cl :asdf))
(in-package :chatai-server-test-asd)

(defsystem chatai-server-test
  :author "stibear"
  :license ""
  :depends-on (:chatai-server
               :prove)
  :components ((:module "t"
                :components
                ((:file "chatai-server"))))
  :perform (load-op :after (op c) (asdf:clear-system c)))
