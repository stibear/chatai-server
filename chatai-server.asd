(in-package :cl-user)
(defpackage chatai-server-asd
  (:use :cl :asdf))
(in-package :chatai-server-asd)

(defsystem chatai-server
  :version "0.1"
  :author "stibear"
  :license ""
  :depends-on (:clack
               :caveman2
               :envy
               :cl-ppcre

               ;; for @route annotation
               :cl-syntax-annot

               ;; HTML Template
               :djula

               ;; for DB
               :datafly
               :sxql

               ;; Shell
               :trivial-shell

               ;; Utilities
               :alexandria

               )
  :components ((:module "src"
                :components
                ((:file "chatai" :depends-on ("main"))
                 (:file "mecab" :depends-on ("chatai"))
                 (:file "chatai-db" :depends-on ("chatai"))
                 (:file "util" :depends-on ("chatai"))
                 (:file "main" :depends-on ("config" "view" "db"))
                 (:file "web" :depends-on ("view"))
                 (:file "view" :depends-on ("config"))
                 (:file "db" :depends-on ("config"))
                 (:file "config"))))
  :description ""
  :in-order-to ((test-op (load-op chatai-server-test))))
