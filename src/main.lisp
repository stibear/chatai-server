(in-package :cl-user)
(defpackage chatai-server
  (:use :cl)
  (:import-from :chatai-server.config
                :config)
  (:import-from :clack
                :clackup)
  (:import-from :alexandria
                :mappend)
  (:export :start
           :stop))
(in-package :chatai-server)

(defvar *appfile-path*
  (asdf:system-relative-pathname :chatai-server #P"app.lisp"))

(defvar *handler* nil)

(defun start (&rest args &key server port debug &allow-other-keys)
  (declare (ignore server port debug))
  (when *handler*
    (restart-case (error "Server is already running.")
      (restart-server ()
        :report "Restart the server"
        (stop))))
  (setf *handler*
        (apply #'clackup *appfile-path* args)))

(defun stop ()
  (prog1
      (clack:stop *handler*)
    (setf *handler* nil)))
