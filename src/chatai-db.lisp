(in-package :chatai-server.chatai)

(defstruct chatai-log
  id sentence meishi h-list rowid)

(defun last-rowid ()
  (with-connection (db)
    (second (retrieve-one (select ((:max :rowid)) (from :log))))))

(defun select-nth (n &optional to)
  (with-connection (db)
    (if to
        (retrieve-all
         (select (:* :rowid) (from :log)
                 (where (:and (:<= n :rowid) (:<= :rowid to))))
         :as 'chatai-log)
        (retrieve-one
         (select (:* :rowid) (from :log)
                 (where (:= :rowid n)))
         :as 'chatai-log))))

(defun select-nth-list (nth-list)
  (with-connection (db)
    (if (> (list-length nth-list) 500)
        (let ((nth-lists (group nth-list 500)))
          (loop
             :for nth-list :in nth-lists
             :append (retrieve-all
                      (select (:* :rowid) (from :log)
                              (where `(:or ,@(loop :for i :in nth-list
                                                :collect (list := :rowid i)))))
                      :as 'chatai-log)))
        (retrieve-all
         (select (:* :rowid) (from :log)
                 (where `(:or ,@(loop :for i :in nth-list
                                   :collect (list := :rowid i)))))
         :as 'chatai-log))))

(defun push-to-db (sentence hiragana-list)
  (with-connection (db)
    (execute (insert-into :log
               (set= :sentence sentence
                     :h-list (format nil "~s" hiragana-list))))))

