(in-package :cl-user)
(defpackage chatai-server.web
  (:use :cl
        :caveman2
        :chatai-server.config
        :chatai-server.view
        :chatai-server.db
        :chatai-server.chatai
        :datafly
        :sxql)
  (:export :*web*))
(in-package :chatai-server.web)

;; for @route annotation
(syntax:use-syntax :annot)

;;
;; Application

(defclass <web> (<app>) ())
(defvar *web* (make-instance '<web>))
(clear-routing-rules *web*)

;;
;; Routing rules

(defroute "/" ()
  (render #P"index.html"))

(defroute ("/chatai" :method :post) (&key |sentence|)
  (let ((chataied
         (handler-case (chatai |sentence| :mode :debug)
           (error () "ERROR"))))
    (format nil "~A" chataied)))

;;
;; Error pages

(defmethod on-exception ((app <web>) (code (eql 404)))
  (declare (ignore app))
  (merge-pathnames #P"_errors/404.html"
                   *template-directory*))
